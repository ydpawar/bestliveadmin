import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActionService, DashboardService, LoginService, UserDataService} from '../../_api/index';
import {AuthIdentityService, ToastrService} from './../../_services/index';
import {Router} from '@angular/router';
import swal from 'sweetalert2';

declare var $;

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css'],
    providers: [LoginService, UserDataService, DashboardService, ActionService, AuthIdentityService]
})
export class HeaderComponent implements OnInit, OnDestroy {

    searchUser: any;
    cUserData: any;
    searchUserData: any;
    searchUserError = false;
    userData: any;
    userBalance: any;
    systemId: string;
    user: any;
    notifyList: any;
    notifyCount: number;
    aList: [];
    isEmpty = false;
    public setlogo: string;
    private isDestroy: boolean = false;
    public isUserBalance: boolean = false;
    public isLoading: boolean = false;

    // tslint:disable-next-line:max-line-length
    constructor(
        private service: UserDataService,
        private service2: DashboardService,
        private service3: ActionService,
        private auth: LoginService,
        private route: Router,
        private authIdentity: AuthIdentityService,
        private toaster: ToastrService) {
        if (authIdentity.isLoggedIn()) {
            this.user = authIdentity.getIdentity();
        }
    }

    ngOnDestroy() {
        this.isDestroy = true;
    }
    ngOnInit() {
        this.systemId = window[ 'operatorId' ];
        this.getUserData();
    }

    async getUserData() {
        await this.service.getUserData().subscribe(
            // tslint:disable-next-line:no-shadowed-variable
            (data) => {
                this.onSuccessUserData(data);
            },
            error => {
                // this.toaster.error('Error in Get User Data Api !!', 'Something want wrong..!');
            });
    }

    onSuccessUserData(response) {
        if (response.status !== undefined) {
            if (response.status === 1) {
                this.userData = response.data;
                window.localStorage.setItem('commentary', response.data.commentary);
                window.localStorage.setItem('lastcomm', response.data.lastcomm);
                window.localStorage.setItem('api_call_timing', JSON.stringify(response.data.api_call_timing));

                // if (!this.isDestroy) {
                //     const xhm1000 = setTimeout(() => { clearTimeout(xhm1000); this.getUserData(); }, 15000);
                // }

                this.notifyCount = response.data.notificationCount;
                this.notifyList = response.data.notificationList;

                if (response.data.notificationList.length <= 0) {
                    this.isEmpty = true;
                }
            }
        }
    }

    getBalance() {
        this.isLoading = true;
        // this.isUserBalance = true;
        // if (this.isUserBalance) {
        //     const xhm5000 = setTimeout(() => { clearTimeout(xhm5000); this.isUserBalance = false; }, 5000);
        // }
        this.service.getUserBalance().subscribe((data) => this.onSuccessGetBalance(data));
    }

    onSuccessGetBalance(response) {
        if (response.status === 1) {
            this.isLoading = false;
            this.userBalance = response.data;
            this.isUserBalance = true;
            if (this.isUserBalance) {
                const xhm5000 = setTimeout(() => { clearTimeout(xhm5000); this.isUserBalance = false; }, 5000);
            }
        }
    }

    logout() {
        swal.fire({
            title: 'Are you sure to logout?',
            // text: 'Are you sure to logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this.auth.doLogout().subscribe((res) => this.onSuccessLogout(res));
            }
        });
    }

    onSuccessLogout(res) {
        this.authIdentity.logOut(this.route);
    }

    submitForm() {
        this.searchUser = $('.user-search-val').val();
        if (this.searchUser) {
            const data = { username: this.searchUser };
            this.service2.searchUserData(data).subscribe((res) => this.onSearch(res));
        }
    }

    onSearch(res) {
        $('.modal-search-desktop').modal('show');
        $('body').removeClass('modal-open');
        if (res.status === 1 && res.data != null) {
            this.searchUserError = false;
            this.searchUserData = res.data;
        } else {
            this.searchUserError = true;
            this.searchUserData = undefined;
        }
    }

    doStatusUpdate(uid, typ) {
        swal.fire({
            title: 'Are you sure to change this status ?',
            // text: 'Are you sure to logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                if (typ === 1) {
                    this.service3.doBlockUnblock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
                } else {
                    this.service3.doLockUnlock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
                }
            }
        });
    }

    doStatusDelete(uid) {
        swal.fire({
            title: 'Are you sure to delete ?',
            // text: 'Are you sure to logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this.service3.doDelete(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
            }
        });
    }

    onSuccessStatusUpdate(res) {
        if (res.status === 1) {
            this.submitForm();
        }
    }
}
