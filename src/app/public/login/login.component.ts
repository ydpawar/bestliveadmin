import {Component, OnInit, AfterViewInit , ViewChild, ElementRef} from '@angular/core';
import {LoginService, UserDataService} from '../../_api/index';
import {Router} from '@angular/router';
import {AuthIdentityService, ToastrService} from './../../_services/index';

declare var $;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [AuthIdentityService, UserDataService]
})

export class LoginComponent implements OnInit, AfterViewInit {
    @ViewChild('recaptcha', {static: false}) recaptcha: ElementRef;
    @ViewChild('apklink', {static: false}) apklink: ElementRef;
    model: any = {};
    public setlogo: string;
    public isreCaptcha: any;
    sitemode: boolean = true;
    disableLogin: boolean = false;
    siteMaintenanceMessage: string = 'Site under maintenance. We will be back shortly!';
    returnUrl: string;
    captcha: string = '------';
    systemId: number;
    apkUrl: string;
    apkDownloadTag: string;
    authId: string;
    passType = 'password';
    passType1 = 'password';
    passType2 = 'password';

    // tslint:disable-next-line:max-line-length
    constructor(
        private auth: LoginService,
        private service: UserDataService,
        private route: Router,
        private authIdentity: AuthIdentityService,
        private toastr: ToastrService
    ) {

    }

    ngOnInit() {
        this.systemId = Number(window['operatorId']);
        this.checkSiteMode();
    }

    ngAfterViewInit() {}


    reloadCaptch(captcha) {
        this.model.recaptcha = '';
        this.auth.siteMode(this.systemId, captcha).subscribe(
            // tslint:disable-next-line:no-shadowed-variable
            (data) => {
                this.onSuccessCheckSiteMode(data);
            },
            error => {
                // tslint:disable-next-line:triple-equals
                // this.toastr.error(error.message, 'Something want to wrong..!');
            });
    }

    async checkSiteMode() {
        await this.auth.siteMode(this.systemId, null).subscribe(
            // tslint:disable-next-line:no-shadowed-variable
            (data) => {
                this.onSuccessCheckSiteMode(data);
            },
            error => {
                // this.toastr.error(error.message, 'Something want to wrong..!');
            });
    }

    onSuccessCheckSiteMode(response) {
        if (response.status !== undefined && response.status === 1) {
            // tslint:disable-next-line:triple-equals
            if (response.data !== undefined && response.data.siteMode == '0') {
                this.sitemode = false;
                this.siteMaintenanceMessage = response.data.message;
            }
            if (response.data.captcha !== undefined) {
                this.captcha = response.data.captcha;
            }
            if (response.data.apkUrl !== undefined) {
                this.apkUrl = response.data.apkUrl;
                this.apkDownloadTag = response.data.apkName;
            }
        }
        this.apklink.nativeElement.setAttribute('href', this.apkUrl);
        this.apklink.nativeElement.setAttribute('download', this.apkDownloadTag);
    }

    doLogin() {
        this.disableLogin = true;
        setTimeout(() => {
            const data = {
                username: this.model.username,
                password: this.model.password,
                systemId: this.systemId,
                recaptcha: this.captcha
            };

            if (data.username && data.password && data.recaptcha) {
                this.auth.doLogin(data).subscribe(
                    // tslint:disable-next-line:no-shadowed-variable
                    (data) => {
                        this.intSuccessLogin(data);
                    },
                    error => {
                        // tslint:disable-next-line:triple-equals
                        this.toastr.error(error.message, 'Something want to wrong..!');
                    });
            } else {
                this.disableLogin = false;
                if (!data.username) {
                    this.toastr.error('Username not be null !!', 'Error:');
                } else if (!data.password) {
                    this.toastr.error('Password not be null !!', 'Error:');
                } else {
                    this.toastr.error('Something want to wrong..!', 'Error:');
                }
            }
        }, 2000);
    }

    intSuccessLogin(response) {
        this.disableLogin = false;
        if (response.status !== undefined && response.status === 1) {
            if (response.data !== undefined) {
                if (response.data.is_password_updated === 1) {
                    this.authIdentity.setUser(response.data);
                    localStorage.setItem('currentUser', JSON.stringify(response.data));
                    window.localStorage.setItem('loadBanner', 'yes');
                    this.route.navigate(['/']);
                } else {
                    this.authId = response.data.auth_id;
                    $('#login-form').hide();
                    $('#change-pass-form').show();
                }

            } else {
                // this.toastr.error(response.error.message, 'Something want wrong..!');
            }
        }
    }

    dochangepass() {
        const data = {
            username: this.model.username,
            password: this.model.password,
            password1: this.model.password1,
            password2: this.model.password2,
        };

        this.auth.dochangepass(data).subscribe(
            // tslint:disable-next-line:no-shadowed-variable
            (data) => {
                this.intSuccesschangepass(data);
            },
            error => {
                // tslint:disable-next-line:triple-equals
                this.toastr.error(error.message, 'Something want to wrong..!');

            });
    }

    intSuccesschangepass(response) {
        if (response.status !== undefined && response.status === 1) {
            $('#login-form').show();
            $('#change-pass-form').hide();
            this.reloadCaptch(null);
        }
    }

    renderReCaptch() {
        window['grecaptcha'].render(this.recaptcha.nativeElement, {
            'sitekey': '6LeKDPcUAAAAANADtXVAZCWnL16Rv4NlcLSu9XnN',
            'theme': 'dark',
            'callback': (response) => {
                // console.log(response);
                this.isreCaptcha = response;
            },
            'expired-callback': (res) => {
                this.isreCaptcha = 0;
                window['grecaptcha'].reset();
            }
        });
    }

    addRecaptchaScript() {
        window['grecaptchaCallback'] = () => {
            this.renderReCaptch();
        };

        (function(d, s, id, obj) {
            let js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                obj.renderReCaptch();
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://www.google.com/recaptcha/api.js?onload=grecaptchaCallback&amp;render=explicit';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'recaptcha-jssdk', this));

    }

}
