import {Component, OnInit, AfterViewInit, HostListener, Renderer2} from '@angular/core';
import {AuthIdentityService, ScriptLoaderService} from './_services/index';
import {LoginService} from './_api';
import {Router} from '@angular/router';
import {compareNumbers, toNumbers} from '@angular/compiler-cli/src/diagnostics/typescript_version';
declare var $;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AuthIdentityService]
})

export class AppComponent implements OnInit, AfterViewInit {
  title = 'BestLive';

  public isInactive = false;
  public userActivityThrottlerTimeout: any = null;
  public userActivityTimeout: any = null;

  constructor(
      private script: ScriptLoaderService,
      private auth: LoginService,
      private route: Router,
      private authIdentity: AuthIdentityService,
      private renderer: Renderer2) {
  }

  ngOnInit() {
    this.getIPV4();
    this.setPlatfrom();
  }
  @HostListener('window:resize', ['$event']) onResize1($event: MouseEvent) {
    this.setPlatfrom();
  }
  @HostListener('mousemove', ['$event']) onMousemove($event: MouseEvent) {
    // console.log('mousemove');
    this.userActivityThrottler();
  }

  @HostListener('scroll', ['$event']) onScroll($event: MouseEvent) {
    // console.log('scroll');
    this.userActivityThrottler();
  }

  @HostListener('keydown', ['$event']) onKeydown($event: MouseEvent) {
    this.userActivityThrottler();
  }

  @HostListener('resize', ['$event']) onResize($event: MouseEvent) {
    this.userActivityThrottler();
  }

  ngAfterViewInit() {
   // this.script.load('script', 'assets/js/vendor.js');
  }

  resetUserActivityTimeout() {
    clearTimeout(this.userActivityTimeout);

    this.userActivityTimeout = setTimeout(() => {
      this.userActivityThrottler();
      this.inactiveUserAction();
    }, 1800000 );
  }

  userActivityThrottler() {
    if (this.isInactive) {
      this.isInactive = false;
      document.getElementsByTagName('body')[0].classList.remove('is-faded');
    }

    if (!this.userActivityThrottlerTimeout) {
      this.userActivityThrottlerTimeout = setTimeout(() => {
        this.resetUserActivityTimeout();
        clearTimeout(this.userActivityThrottlerTimeout);
        this.userActivityThrottlerTimeout = null;
      }, 10000);
    }
  }

  inactiveUserAction() {
    const isLoggedIn = (localStorage.getItem('1e846c20ec61136967c0727684fa7ed8') != null);
    if (isLoggedIn === false) {
      return null;
    }
    this.isInactive = true;
    // console.log('Inactive-', this.isInactive);
    // document.getElementsByTagName('body')[0].classList.add('is-faded');
    const result = confirm('You are inactive for longtime (inactive timeout).');
    if (result) {
      // this._router.navigate(['/auth/']);
      this.auth.doLogout().subscribe((res) => this.onSuccessLogout(res));
    } else {
      window.location.reload();
    }
  }

  onSuccessLogout(res) {
    this.authIdentity.logOut(this.route);
  }

  setPlatfrom() {
    if (window.innerWidth < 992) {
      // this.renderer.setAttribute(document.body, 'data-sidebar-size', 'default');
      this.renderer.setAttribute(document.body, 'data-platform', 'mobile');
    } else {
      this.renderer.setAttribute(document.body, 'data-platform', 'desktop');
    }
  }

  getIPV4() {
    $.getJSON('https://api.ipify.org?format=jsonp&callback=?',
      (json) => {
        localStorage.setItem('ip', json.ip);
      }
    );
  }

}
