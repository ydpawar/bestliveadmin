import { Injectable } from '@angular/core';
import { LocalStorage } from './local-storage.service';
import { Md5 } from 'ts-md5/dist/md5';
import { Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';

export class User {
  username: string;
  email: string;
  token: string;
  role: string;
  allowed_resources: string[];
}

@Injectable()
export class AuthIdentityService {
  user: User = new User;
  // public authKey: string = 'currentUser';
  public authKey: string = Md5.hashStr('dreamexch', false).toString();
  public LocalStorage: LocalStorage = new LocalStorage();

  constructor() {

  }

  isLoggedIn(): boolean {
    return this.LocalStorage.getItem(this.authKey) !== null;
    // return this.LocalStorage.getItem(this.authKey) !== null;
  }

  logOut(router?: Router) {
    localStorage.removeItem('currentUser');
    this.LocalStorage.removeItem(this.authKey);
    if (router !== undefined) {
      router.navigate(['/login']);
    }
  }

  setUser(user: User): void {
    this.LocalStorage.removeItem(this.authKey);
    this.LocalStorage.setItem(this.authKey, JSON.stringify(user));
  }

  getIdentity(): User {
    if (!this.isLoggedIn()) {
      return null;
    } else {
      let storedUser: string, storedUserObj;

      storedUser = this.LocalStorage.getItem(this.authKey);
      storedUserObj = JSON.parse(storedUser);

      this.user = storedUserObj;

      return this.user;
    }
  }

  setAuthToken(token: string): void {
    this.LocalStorage.setItem('tempAuthToken', token);
  }

  getAuthToken(): string {
    return this.LocalStorage.getItem('tempAuthToken');
  }

  removeAuthToken(): void {
    this.LocalStorage.removeItem('tempAuthToken');
  }
}
