import {AfterViewInit, Component, OnInit} from '@angular/core';

declare var $;

// @ts-ignore
@Component({
    selector: 'app-theme',
    templateUrl: './theme.component.html',
    styleUrls: ['./theme.component.css']
})
export class ThemeComponent implements OnInit, AfterViewInit {

    commentaryData: string = 'This is global commentary !!';
    lastcommData: any;
    loaderColorArr = { 'theme-design-default' : '#00b1ff', 'theme-design-1' : '#ffbc00' ,'theme-design-2' : '#ff0000' ,'theme-design-3' : '#00a6b7','theme-design-4' : '#277349'  ,'theme-design-5' : '#fd5d00'};
    loaderColor: string = '#00B1FF';

    constructor() {
    }

    ngOnInit() {
        if ( window[ 'themeName' ] ) {
            const theme = 'theme-design-' + window[ 'themeName' ];
            this.loaderColor = this.loaderColorArr[ theme ];
        }
    }


    ngAfterViewInit(): void {
        const t = {
            $body: $('body'), $window: $(window)
        };
        var i = this;
        const $body = $('body'), $window = $(window);
        var e = $;
        e('.button-menu-mobile').on('click', function(t) {
            t.preventDefault(), $body.toggleClass('sidebar-enable'), 768 <= $window.width() ? $body.toggleClass('enlarged') : $body.removeClass('enlarged'), i._resetSidebarScroll();
        }), e('#side-menu').metisMenu(), i._resetSidebarScroll(), e('.right-bar-toggle').on('click', function(t) {
            e('body').toggleClass('right-bar-enabled');
        }), e(document).on('click', 'body', function(t) {
            0 < e(t.target).closest('.right-bar-toggle, .right-bar').length || 0 < e(t.target).closest('.left-side-menu, .side-nav').length || e(t.target).hasClass('button-menu-mobile') || 0 < e(t.target).closest('.button-menu-mobile').length || (e('body').removeClass('right-bar-enabled'), e('body').removeClass('sidebar-enable'));
        }), e('#side-menu a').each(function() {
            var t = window.location.href.split(/[?#]/)[0];
            this.href == t && (e(this).addClass('active'), e(this).parent().addClass('active'), e(this).parent().parent().addClass('in'), e(this).parent().parent().prev().addClass('active'), e(this).parent().parent().parent().addClass('active'), e(this).parent().parent().parent().parent().addClass('in'), e(this).parent().parent().parent().parent().parent().addClass('active'));
        }), e('.navbar-toggle').on('click', function(t) {
            e(this).toggleClass('open'), e('#navigation').slideToggle(400);
        }), e(window).on('load', function() {
            e('#status').fadeOut(), e('#preloader').delay(350).fadeOut('slow');
        });

        setTimeout(() => {
            this.commentaryData = window.localStorage.getItem('commentary');
            this.lastcommData = window.localStorage.getItem('lastcomm');
        }, 1000);

    }

    _resetSidebarScroll() {
        $('.slimscroll-menu').slimscroll({
            height: 'auto',
            position: 'right',
            size: '8px',
            color: '#9ea5ab',
            wheelStep: 5,
            touchScrollStep: 20
        });
    }

}


