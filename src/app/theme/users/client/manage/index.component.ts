import {Component, OnInit, AfterViewInit, OnDestroy, Injector} from '@angular/core';
import {ActionService, ClientService} from '../../../../_api/index';
import {AuthIdentityService} from '../../../../_services/index';
import {BaseComponent} from '../../../../common/commonComponent';

declare var $, swal;

interface User {
    id: string;
    name: string;
    username: string;
    email: string;
    mobile: string;
    plBalance: string;
    balance: string;
    mc: string;
    expose: string;
    available: string;
    pName: string;
    remark: string;
    isLogin: string;
    isActive: string;
    isBlock: {
        text: string;
        class1: string;
        class2: string;
    };
    isLock: {
        text: string;
        class1: string;
        class2: string;
    };
}

class UserObj implements User {
    id: string;
    name: string;
    username: string;
    email: string;
    mobile: string;
    plBalance: string;
    balance: string;
    mc: string;
    expose: string;
    available: string;
    pName: string;
    remark: string;
    isLogin: string;
    isActive: string;
    isBlock: {
        text: string;
        class1: string;
        class2: string;
    };
    isLock: {
        text: string;
        class1: string;
        class2: string;
    };
}


@Component({
    selector: 'app-user',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css'],
    providers: [ClientService, ActionService]
})
export class ManageComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {

    page = {start: 1, end: 5};
    user: any;
    aList: User[] = [];
    cItem: User;
    isEmpty = false;
    isLoading = false;
    isLoadMore = true;
    userCount: string = '0';
    loadCount: string = '0';
    dataTableId = 'DataTables_client';
    title = 'Client';
    createUrl = '/users/client/create';
    breadcrumb: any = [{title: 'Client', url: '/'}, {title: 'Manage', url: '/'}];

    public notscrolly: boolean = true;
    public notEmptyPost: boolean = true;
    public pageNo: number = 1;

    constructor(
        inj: Injector,
        private service: ClientService,
        private service2: ActionService) {
        super(inj);
        const auth = new AuthIdentityService();
        if (auth.isLoggedIn()) {
            this.user = auth.getIdentity();
        }
    }

    ngOnInit() {
        this.spinner.show();
        this.createForm();
        this.applyFilters();
    }

    ngAfterViewInit() {
    }

    ngOnDestroy() {
        window.localStorage.removeItem(this.dataTableId);
    }

    // onScroll() {
    //     if (this.notEmptyPost && this.isLoadMore && this.notscrolly) {
    //         this.notscrolly = false;
    //         this.pageNo++;
    //         this.applyFilters();
    //     }
    // }

    applyFilters() {
        // const data = {page: this.pageNo};
        this.service.manage().subscribe((res) => this.onSuccess(res));
    }

    onSuccess(res) {

        if (res.status !== undefined && res.status === 1) {
            if (res.data !== undefined && res.data !== undefined) {
                $('#DataTables_client').dataTable().fnDestroy();
                const items = res.data;
                const data: User[] = [];
                // if (items.length === 0) {
                //     this.notEmptyPost = false;
                // } else {
                //     this.notEmptyPost = true;
                // }
                if (items.length > 0) {
                    this.aList = [];
                    // this.loadCount = res.data.loadCount;
                    // this.userCount = res.data.count;
                    for (const item of items) {
                        const cData: User = new UserObj();

                        cData.id = item.id;
                        cData.name = item.name;
                        cData.username = item.username;
                        cData.email = item.email;
                        cData.mobile = item.mobile;
                        cData.plBalance = item.pl_balance;
                        cData.mc = item.mc;
                        cData.balance = item.balance;
                        cData.expose = item.expose;
                        cData.available = item.available;
                        cData.remark = item.remark;
                        cData.pName = item.pName;
                        cData.isActive = item.isActive;
                        cData.isBlock = {
                            text: item.isBlock === 1 ? 'Unblock' : 'Block',
                            class1: item.isBlock === 1 ? 'success' : 'danger',
                            class2: item.isBlock === 1 ? 'mdi-check' : 'mdi-close'
                        };
                        cData.isLock = {
                            text: item.isLock === 1 ? 'Bet Unlock' : 'Bet Lock',
                            class1: item.isLock === 1 ? 'success' : 'danger',
                            class2: item.isLock === 1 ? 'mdi-toggle-switch' : 'mdi-toggle-switch-off'
                        };

                        data.push(cData);
                        // this.aList.push(cData);
                    }
                } else {
                    this.isEmpty = true;
                }

                this.aList = data;
                this.page.end = this.page.end + items.length - 1;
                this.initDataTables('DataTables_client');
                // this.notscrolly = true;
            }
        }
        this.spinner.hide();
    }

    doStatusUpdate(uid, typ) {
        swal.fire({
            title: 'Are you sure to change this status ?',
            // text: 'Are you sure to logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                if (typ === 1) {
                    this.service2.doBlockUnblock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
                } else {
                    this.service2.doLockUnlock(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
                }
            }
        });
    }

    doStatusDelete(uid) {
        swal.fire({
            title: 'Are you sure to delete ?',
            // text: 'Are you sure to logout?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                this.service2.doDelete(uid).subscribe((res) => this.onSuccessStatusUpdate(res));
            }
        });
    }

    onSuccessStatusUpdate(res) {
        if (res.status === 1) {
            // this.aList = []; // this.pageNo = 1;
            this.applyFilters();
        }
    }

    createForm() {
        this.frm = this.formBuilder.group({
            client: [''],
        });
    }

    submitForm() {
        // if (this.frm.valid) {
        this.spinner.show();
        const data = this.frm.value;
        this.service.manage().subscribe((res) => this.onSearch(res));
        // }
    }

    onSearch(res) {
        if (res.status === 1) {
            this.spinner.hide();
            // this.aList = [];
            // this.isLoadMore = false;
            this.onSuccess(res);
        }
    }

    get frmClient() { return this.frm.get('client'); }
}

