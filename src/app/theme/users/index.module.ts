import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { UsersComponent } from './index.component';

const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    children: [
      {
        path: 'super-admin',
        loadChildren: './super-admin/index.module#SuperAdminModule'
      },
      {
        path: 'sub-admin',
        loadChildren: './sub-admin/index.module#SubAdminModule'
      },
      {
        path: 'supervisor',
        loadChildren: './supervisor/index.module#SupervisorModule'
      },
      {
        path: 'super-master',
        loadChildren: './super-master/index.module#SuperMasterModule'
      },
      {
        path: 'master',
        loadChildren: './master/index.module#MasterModule'
      },
      {
        path: 'client',
        loadChildren: './client/index.module#ClientModule'
      },
      {
        path: 'action',
        loadChildren: './action/index.module#ActionModule'
      },
      {
        path: 'manage-system',
        loadChildren: './manage-system/index.module#ManageSystemModule'
      },
      {
        path: '',
        redirectTo: 'dashbord',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
  declarations: [UsersComponent],
})
export class UsersModule {
}
