import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import {ActionService} from '../../../../_api/index';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {AuthIdentityService} from '../../../../_services';

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [ActionService]
})
export class ChangePasswordComponent implements OnInit, AfterViewInit, OnDestroy {

  title = 'Users - Change Password';
  breadcrumb: any = [{title: 'Users', url: '/' }, {title: 'Change Password', url: '/' }];

  uid: string;
  frm: FormGroup;
  frm2: FormGroup;
  cUserData: any;
  passType = 'password';
  passType1 = 'password';
  passType2 = 'password';

  constructor(
      private formBuilder: FormBuilder,
      private service: ActionService,
      private router: Router,
      private route: ActivatedRoute,
      private authIdentity: AuthIdentityService,
      // tslint:disable-next-line:variable-name
      private _location: Location
  ) {
    this.uid = this.route.snapshot.params.uid;
    // tslint:disable-next-line:triple-equals
    if ( window.localStorage.getItem('title') != null && window.localStorage.getItem('title') != undefined ) {
      this.title = 'Reset Password - ' + window.localStorage.getItem('title');
    }
  }

  ngOnInit() {
    this.createForm();
    this.createForm2();
    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.cUserData = auth.getIdentity();
    }
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem('title');
  }

  validatePassword(control: AbstractControl): { [key: string]: any } | null {
    if (control.value) {
      const regex = /\d+/g;
      const onlyLetters = /[a-zA-Z]/;
      if (control.value !== '' || control.value !== undefined || control.value !== null) {
        if (!control.value.match(regex)) {
          return {passwordInvalid: true};
        }
        if (!control.value.match(onlyLetters)) {
          return {passwordInvalid: true};
        }
      }
      return null;
    }
    return null;
  }

  createForm() {
    this.frm = this.formBuilder.group({
      uid: [this.uid],
      password: ['', [Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20),
       this.validatePassword
      ]],
    });
  }

  createForm2() {
    this.frm2 = this.formBuilder.group({
      password1: ['', [Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20),
        this.validatePassword
      ]],
      password2: ['', [Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20),
        this.validatePassword
      ]],
    });
  }

  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.service.resetPassword(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.authIdentity.logOut(this.router);
    }
  }

  submitForm2() {
    const data = this.frm2.value;
    if (this.frm2.valid) {
      // this.frm2.reset();
      this.service.resetActivityPassword(data).subscribe((res) => this.onSuccess2(res));
    }
  }

  onSuccess2(res) {
    if (res.status === 1) {
      this.frm2.reset();
    }
  }

  onCancel() {
    this._location.back();
  }

  getFormData() {
    const data = this.frm.value;
    return data;
  }

  get frmPassword() { return this.frm.get('password'); }
  get frmPassword1() { return this.frm2.get('password1'); }
  get frmPassword2() { return this.frm2.get('password2'); }

}

