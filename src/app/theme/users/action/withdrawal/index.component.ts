import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import { ActionService } from '../../../../_api/index';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [ActionService]
})
export class WithdrawalComponent implements OnInit, AfterViewInit, OnDestroy {

  title = 'Users - Withdrawal';
  breadcrumb: any = [{title: 'Users', url: '/' }, {title: 'Withdrawal', url: '/' }];

  uid: string;
  frm: FormGroup;
  userError: string;
  parentUser: string;
  thisUser: string;
  pBalance: number;
  cBalance: number;
  tmpbalance: number;
  tmpbalance2: number;
  inputValue: any;
  reqId: any;

  constructor(
      private service: ActionService,
      private formBuilder: FormBuilder,
      private router: Router,
      private route: ActivatedRoute,
      // tslint:disable-next-line:variable-name
      private _location: Location
      ) {
        this.createForm(); 

        this.uid = this.route.snapshot.params.uid;
        if(this.uid != null && this.uid != undefined){this.frm.patchValue({ uid: this.uid });}
    
        if (window.sessionStorage.getItem('inputValue') != null && window.sessionStorage.getItem('inputValue') != undefined && window.sessionStorage.getItem('reqId') != undefined) {
          this.inputValue = window.sessionStorage.getItem('inputValue');
          this.reqId = window.sessionStorage.getItem('reqId');
          this.frm.patchValue({uid:this.uid,balance: this.inputValue, withdraw_req_id : this.reqId});
          window.sessionStorage.removeItem('inputValue');
          window.sessionStorage.removeItem('reqId');
          console.log(this.inputValue,this.reqId);
        }
  }

  ngOnInit() {
    this.checkUserData();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
  }

  createForm() {
    this.frm = this.formBuilder.group({
      uid: [this.uid],
      balance: ['', Validators.required],
      withdraw_req_id: [this.reqId],
      utr_number: [''],
      remark: ['']
    });
  }

  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.frm.reset();
      this.frm.patchValue({uid: this.uid });
      this.service.withdrawalBalance(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this._location.back();
    }
  }

  checkUserData() {
    this.service.checkUserDataBoth(this.uid).subscribe((res) => {
      this.intCheckUser(res);
    });
  }

  intCheckUser(res) {
    if (res.status === 1) {
      this.userError = '';
      // tslint:disable-next-line:triple-equals
      if ( res.data.pData != undefined ) {
        this.pBalance = this.tmpbalance = res.data.pData.balance;
        this.parentUser = res.data.pData.username;
        if(this.inputValue){
          this.pBalance = Number(this.pBalance) + Number(this.inputValue);
        }
      }
      // tslint:disable-next-line:triple-equals
      if ( res.data.cData != undefined ) {
        this.cBalance = this.tmpbalance2 = res.data.cData.balance;
        this.thisUser = res.data.cData.username;
        this.title = 'Withdrawal from ' + this.thisUser;
        if(this.inputValue){
          this.cBalance = Number(this.cBalance) - Number(this.inputValue);
        }
      }
    }
    if (res.status === 0) {
      // tslint:disable-next-line:triple-equals
      if (res.success != undefined) {
        this.userError = res.success.message;
      }
    }
  }

  checkBalance(balance: any) {
    this.pBalance = Number(this.tmpbalance) + Number(balance.value);
    this.cBalance = Number(this.tmpbalance2) - Number(balance.value);
  }

  onCancel() {
    this._location.back();
  }

  getFormData() {
    const data = this.frm.value;
    return data;
  }

  get frmBalance() { return this.frm.get('balance'); }
  get frmRemark() { return this.frm.get('remark'); }
  get frmUtrNo() { return this.frm.get('utr_number'); }


}

