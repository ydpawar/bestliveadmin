import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SuperAdminComponent } from './index.component';

import {ManageComponent} from './manage/index.component';
import {CreateComponent} from './create/index.component';

import {ManageModule} from './manage/index.module';
import {CreateModule} from './create/index.module';



const routes: Routes = [
  {
    path: '',
    component: SuperAdminComponent,
    children: [
      {
        path: '',
        component: ManageComponent
      },
      {
        path: 'manage',
        component: ManageComponent
      },
      {
        path: 'create',
        component: CreateComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule,
    ManageModule, CreateModule
  ], exports: [
    RouterModule
  ], declarations: [ SuperAdminComponent ]
})
export class SuperAdminModule {

}
