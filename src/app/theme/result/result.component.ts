import { AfterViewInit, Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, MaxLengthValidator, Validators } from '@angular/forms';
import { BaseComponent } from 'src/app/common/commonComponent';
import { RulesService } from 'src/app/_api';
import { ResultService } from 'src/app/_api/result/result.service';
import { AuthIdentityService } from 'src/app/_services';
import {Location} from '@angular/common';
import swal from 'sweetalert2';

import * as moment from 'moment'; // Momentjs
declare var $;
// tslint:disable-next-line:class-name
interface listData {
  marketId: string;
  marketName: string
  eventName: string;
  eventId: string;
  date: string;
  result: string;
  description: string;
  game_over: number;
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
  marketId: string;
  marketName: string
  eventName: string;
  eventId: string;
  date: string;
  result: string;
  description: string;
  game_over: number;
}

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css'],
  providers: [ResultService]
})
export class ResultComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {

  frm: FormGroup;
  title = 'Result';
  breadcrumb: any = [{ title: 'Result', url: '/' }, { title: 'List', url: '/' }];
  page = { start: 1, end: 5 };

  user: any;
  uid = '';
  aList: any[] = [];
  aListTotal: any;
  cItem: any;
  isEmpty = false;
  isLoading = false;
  isDefault = true;
  dataTableId = 'DataTables_result';
  public filter: any ={};
  private isDestroy: boolean = false;
  fType: string ='';
  constructor(
    inj: Injector,
    private service: ResultService,
    private _location: Location
  ) {
    super(inj);
    this.uid = this.activatedRoute.snapshot.params.uid;

    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
      if ( this.user.role != 'ADMIN') {
        this. _location.back();
      }
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.createForm();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
    this.isDestroy = true;
  }

  filters(search: string) {
    this.fType = search;
    this.filter.isFirst = 0;
    switch (search) {
      case 'today' :
        this.filter.start_date = moment().format('YYYY-MM-DD');
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'yesterday' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(1, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'week' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(7, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'month' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(30, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case '3_month' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(90, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
    }
    // this.filterChanged.emit(this.filter);
  }

  setInputDate(sDate: Date, eDate: Date) {
      const data={'start': sDate ,'end' : eDate}
      this.frm.patchValue(data);
      this.submitForm(this.fType);
  }

  applyFilters() {
    this.isDefault = false;
    const data = this.frm.value; data.isFirst = 1; data.ftype = 'today';
    this.service.result(this.uid, data).subscribe((res) => this.onSuccess(res));

    if (!this.isDestroy) {
        const xhm1000 = setTimeout(() => { clearTimeout(xhm1000); this.applyFilters(); }, 30000);
     }
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1) {
      $('#DataTables_result').dataTable().fnDestroy();
      const data = res.data;
      if (res.userName !== undefined) {
        this.title = 'Result for ' + res.userName;
      }
      if (res.data !== 'null') {
        this.aList = res.data;
      } else {
        this.isEmpty = true;
      }
      this.page.end = this.page.end + this.aList.length - 1;
      this.initDataTables('DataTables_result');
    }

    this.spinner.hide();
  }

  createForm() {
    this.frm = this.formBuilder.group({
      start: ['', [Validators.required]],
      end: ['', [Validators.required]],
      // sysId: ['0'],
    });
  }

  submitForm(fType = null) {
    this.isDefault = false;
    if (fType != null) {
      this.spinner.show();
      const data = this.frm.value; data.ftype = fType; data.isFirst = 0
      this.service.result(this.uid, data).subscribe((res) => this.onSearch(res));
    } else {
      if (this.frm.valid) {
        this.spinner.show();
        const data = this.frm.value; data.isFirst = 0
        this.service.result(this.uid, data).subscribe((res) => this.onSearch(res));
      }
    }
  }

  onSearch(res) {
    if (res.status === 1) {
      // this.frm.reset();
      this.onSuccess(res);
    }
  }

  get frmStart() { return this.frm.get('start'); }
  get frmEnd() { return this.frm.get('end'); }

  gameOver(item) {
    if (item.showRunner === 0) {
    swal.fire({
      title: 'Confirm Game Over?',
      // text: title,
      input: 'text',
      inputAttributes: {
        autocapitalize: 'off',
        placeholder: 'Winner Result',
        type: 'number',
        pattern: '[0-9]*',
      },
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'info',
      // buttons: ['Cancel', 'Yes'],
    })
      .then((result) => {
        if (result.value) {
          this.initGameOver(item.eventId, item.marketId, result, item.mType);
        }
      });
    }

    if (item.showRunner === 1) {
      const runner = JSON.parse(item.runners);
      let myArrayOfThings;
      if (runner[2] && runner[2].secId) {
        myArrayOfThings = [
          { id: runner[0].secId + '_' + runner[0].runner, name: runner[0].runner },
          { id: runner[1].secId + '_' + runner[1].runner, name: runner[1].runner },
          { id: runner[2].secId + '_' + runner[2].runner, name: runner[2].runner }
        ];
      } else {
        myArrayOfThings = [
          { id: runner[0].secId + '_' + runner[0].runner, name: runner[0].runner },
          { id: runner[1].secId + '_' + runner[1].runner, name: runner[1].runner },
        ];
      }

      const options = {};
      $.map(myArrayOfThings,
        // tslint:disable-next-line:only-arrow-functions
        function (o) {
          options[o.id] = o.name;
        });
      swal.fire({
        title: 'Confirm Game Over?',
        // text: title,
        input: 'select',
        inputOptions: options,
        // content: select,
        showCancelButton: true,
        confirmButtonText: 'Yes, Do it!',
        icon: 'info',
        // buttons: ['Cancel', 'Yes'],
      })
        .then((result) => {
          if (result.value) {
            this.initGameOver(item.eventId, item.marketId, result, item.mType);
          }
        });
    }

  }

  initGameOver(id, mId, results , mType) {
    const data = { eventId: id, marketId: mId, winResult: results.value , mType: mType };
    this.service.gameOver(data).subscribe((res) => this.onGameOver(res));
  }

  onGameOver(res) {
    if (res.status === 1) {
      // this.toastr.success(res.success.message);
      this.applyFilters();
    } else {
      // this.toastr.error(res.error.message);
    }
  }

  gameRecall(id, eid,mType) {
    swal.fire({
      title: 'Confirm Game Recall?',
      // text: title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'warning',
      // buttons: ['Cancel', 'Yes'],
    }).then((result) => {
      if (result.value) {
        this.initGameRecall(id, eid, mType);
      }
    });
  }

  initGameRecall(id, eid ,mType) {
    const data = { marketId: id, eventId: eid ,mType:mType};
    this.service.gameRecall(data).subscribe((res) => this.onGameOver(res));
  }


  abundant(item) {
    swal.fire({
      title: 'Confirm Game Abundant?',
      // text: title,
      showCancelButton: true,
      confirmButtonText: 'Yes, Do it!',
      icon: 'warning',
    })
      .then((result) => {
        if (result.value) {
          this.initGameAbundant(item.marketId, item.eventId ,item.mType);
        }
      });
  }

  initGameAbundant(mid, eid ,mType) {
    const data = { marketId: mid, eventId: eid, mType:mType };
    this.service.abundant(data).subscribe((res) => this.onGameOver(res));
  }
}
