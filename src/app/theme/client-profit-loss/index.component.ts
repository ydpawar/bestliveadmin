import {AfterViewInit, Component, OnDestroy, OnInit, Injector} from '@angular/core';
import {SummaryService} from '../../_api';
import {AuthIdentityService} from '../../_services';
import {FormGroup, Validators} from '@angular/forms';
import {BaseComponent} from '../../common/commonComponent';
import * as moment from 'moment';

declare var $;
// tslint:disable-next-line:class-name
interface userData {
  id: string;
  pid: string;
  name: string;
  role: string;
  pl: string;
  a: string;
  sm1: string;
  sm2: string;
  m1: string;
  c: string;
}

// tslint:disable-next-line:class-name
class userDataObj implements userData {
  id: string;
  pid: string;
  name: string;
  role: string;
  pl: string;
  a: string;
  sm1: string;
  sm2: string;
  m1: string;
  c: string;
}

declare var $;

@Component({
  selector: 'app-home',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [SummaryService]
})

export class ClientProfitLossComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
  frm: FormGroup;
  page = { start: 1, end: 5 };
  title = 'Client Profit Loss';
  breadcrumb: any = [{title: 'Client Profit Loss', url: '/' }];
  uid = '';
  user: any;
  aListTotal: any;
  aList: userData[] = [];
  aList1Total: any;
  cItem: userData;
  isEmpty = false;
  isLoading = false;
  cUserName: string;
  pUserId: string;
  SettleType: string;
  errorSummary: string;
  userBalance:number = 0;
  isBack: boolean = false;
  isCanSettle: boolean = false;
  isCheckBox: boolean = false;
  isDefault = true;
  public filter: any ={};
  fType :string ='';
  // tslint:disable-next-line:max-line-length
  constructor(
      inj: Injector,
      private service: SummaryService,
  ) {
    super(inj);
    this.uid = this.activatedRoute.snapshot.params.uid;

    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }

  }

  ngOnInit() {
    this.spinner.show();
    this.createForm();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
  }

  applyFilters() {
    const data = this.frm.value;
    this.service.clientProfitLoss(this.uid, data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1) {
      if (res.cUserData !== undefined) {
        this.cUserName = res.cUserData.cUserName;
        this.pUserId =  res.cUserData.pid;
        this.isBack =  res.cUserData.isBack;
        this.isCanSettle = res.cUserData.isCanSettle;
      }
      if (res.data !== undefined && res.data !== undefined) {
        $('#ScrollVertical').dataTable().fnDestroy();
        const items = res.data;
        this.aListTotal = res.totalData;

        const data: userData[] = [];
        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: userData = new userDataObj();
            cData.id = item.uid;
            cData.pid = item.pid;
            cData.name = item.name;
            cData.role = item.role;
            cData.pl = item.pl;
            cData.a = item.a;
            cData.sm1 = item.sm1;
            cData.sm2 = item.sm2;
            cData.m1 = item.m1;
            cData.c = item.c;
            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        this.loadScript();
        this.spinner.hide();
      }
    }
  }

  goToUserSummary(usr) {
    const data = this.frm.value;
    this.service.clientProfitLoss(usr.id, data).subscribe((res) => this.onSuccess(res));
  }

  goToUserSummaryBack(pid) {
    const data = this.frm.value;
    this.service.clientProfitLoss(pid,data).subscribe((res) => this.onSuccess(res));
  }

  loadScript() {
    // tslint:disable-next-line:only-arrow-functions
    $(document).ready( function() {
      $('#ScrollVertical').DataTable({
        "scrollX": true,
        "destroy": true,
        "retrieve": true,
        "stateSave": true,
        "paging": false
      }); // Complex headers with column visibility Datatable
    });

  }

  createForm() {
    this.frm = this.formBuilder.group({
      start: ['', Validators.required],
      end: ['', Validators.required],
    });
  }

  submitForm( fType = null ) {
    this.isDefault = false;
    if ( fType != null ) {
      this.spinner.show();
      const data = this.frm.value; data.ftype = fType;
      this.service.clientProfitLoss(this.uid, data).subscribe((res) => this.onSearch(res));
    } else {
      if (this.frm.valid) {
        this.spinner.show();
        const data = this.frm.value;
        this.service.clientProfitLoss(this.uid, data).subscribe((res) => this.onSearch(res));
      }
    }
  }

  onSearch(res) {
    if (res.status === 1) {
      // this.frm.reset();
      this.onSuccess(res);
    }
  }

  get frmStart() { return this.frm.get('start'); }
  get frmEnd() { return this.frm.get('end'); }

  filters(search: string) {
    this.fType = search;
    this.filter.isFirst = 0;
    switch (search) {
      case 'today' :
        this.filter.start_date = moment().format('YYYY-MM-DD');
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'yesterday' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(1, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'week' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(7, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'month' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(30, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case '3_month' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(90, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
    }
    // this.filterChanged.emit(this.filter);
  }

  setInputDate(sDate: Date, eDate: Date) {
    const data={'start': sDate ,'end' : eDate}
    this.frm.patchValue(data);
    this.submitForm(this.fType);
  }

}
