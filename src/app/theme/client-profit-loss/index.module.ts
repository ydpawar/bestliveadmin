import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClientProfitLossComponent } from './index.component';


const routes: Routes = [
  {
    path: '',
    component: ClientProfitLossComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    ClientProfitLossComponent
  ]
})
export class ClientProfitLossModule {
}
