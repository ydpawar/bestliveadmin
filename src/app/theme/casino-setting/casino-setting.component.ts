import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { SettingService } from 'src/app/_api';

declare var $;
interface CasinoSetting {
  id: string;
  game_id: string;
  desciption: string;
  max_stack: string;
}

class CasinoSettingObj implements CasinoSetting {
  id: string;
  game_id: string;
  desciption: string;
  max_stack: string;
}


@Component({
  selector: 'app-casino-setting',
  templateUrl: './casino-setting.component.html',
  styleUrls: ['./casino-setting.component.css'],
  providers: [SettingService]
})

export class CasinoSettingComponent implements OnInit, AfterViewInit, OnDestroy {
  frm: FormGroup;
  page = { start: 1, end: 5 };

  aList: CasinoSetting[] = [];
  cItem: CasinoSettingObj;
  keyName: string;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_setting_casino';

  title = 'Casino Setting';
  createUrl = '/casino-setting';
  breadcrumb: any = [{ title: 'Casino Setting', url: '/' }];

  constructor(
    private formBuilder: FormBuilder,
    private service: SettingService,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
    this.createForm();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    this.service.getCasinoSetting().subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (res.status !== undefined && res.status === 1) {
      if (res.data !== undefined && res.data !== undefined) {
        $('#DataTables_setting_casino').dataTable().fnDestroy();
        const items = res.data;
        const data: CasinoSetting[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: CasinoSetting = new CasinoSettingObj();
            cData.id = item._id;
            cData.game_id = item.game_id;
            cData.desciption = item.product + ' -> ' + item.name;
            cData.max_stack = item.max_stack;

            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        // this.loadJs.load('script' , 'assets/js/datatables.init.js');
        this.loadScript();
      }
    }
    this.spinner.hide();
  }

  updateSetting(item) {
    this.frm.reset();
    $('.clear-settlement').modal('show');
    $('body').removeClass('modal-open');

    this.keyName = item.desciption;

    this.frm.patchValue({
      maxstack: item.max_stack,
      id: item.id.$oid
    });

  }

  createForm() {
    this.frm = this.formBuilder.group({
      maxstack: ['', Validators.required],
      id: [''],
    });
  }

  submitForm() {
    if (this.frm.valid) {
      const data = this.frm.value;
      this.service.updateCasinoSetting(data).subscribe((res) => this.onSubmit(res));
    }
  }

  onSubmit(res) {
    console.log(res)
    if (res.status === 1) {
      console.log('hjh')
      $('.clear-settlement').modal('hide');
      $('body').removeClass('modal-open');
      this.applyFilters();
    }
  }

  loadScript() {
    // tslint:disable-next-line:only-arrow-functions
    $(document).ready(function () {
      $('#DataTables_setting_casino').DataTable({
        "scrollX": true,
        "destroy": true,
        "retrieve": true,
        "stateSave": true,
        "language": {
          "paginate": {
            "previous": "<i class='mdi mdi-chevron-left'>",
            "next": "<i class='mdi mdi-chevron-right'>"
          }
        },
        "drawCallback": function drawCallback() {
          $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
      }); // Complex headers with column visibility Datatable
    });

  }

  get frmValue() { return this.frm.get('maxstack'); }

}

