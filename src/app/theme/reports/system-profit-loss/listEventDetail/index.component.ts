import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import {HistoryService} from '../../../../_api/index';
import {AuthIdentityService, ScriptLoaderService} from '../../../../_services';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ExcelService} from '../../../../common/excel.service';
import {NgxSpinnerService} from 'ngx-spinner';
import * as moment from 'moment'; // Momentjs
declare var $;
// tslint:disable-next-line:class-name
interface listData {
  id: string;
  time: string;
  description: string;
  eventId: string;
  pl: string;
  comm: string;
  result: string;
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
  id: string;
  time: string;
  description: string;
  eventId: string;
  pl: string;
  comm: string;
  result: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [HistoryService]
})

export class ListEventDetailComponent implements OnInit, AfterViewInit, OnDestroy {

  frm: FormGroup;
  title = 'Event - System Profit Loss';
  breadcrumb: any = [{title: 'System Profit Loss', url: '/' }, {title: 'List', url: '/' }];

  page = { start: 1, end: 5 };

  user: any;
  uid = '';
  sid: string;
  aList: listData[] = [];
  aListTotal: any;
  cItem: listData;
  isEmpty = false;
  isLoading = false;
  isDefault = true;
  dataTableId = 'DataTables_profit_loss_event';
  public filter: any ={};
  fType :string ='';
  constructor(
      private formBuilder: FormBuilder,
      private service: HistoryService,
      private loadJs: ScriptLoaderService,
      private route: ActivatedRoute,
      private excelSheet: ExcelService,
      private spinner: NgxSpinnerService
  ) {
    this.sid = this.route.snapshot.params.sid;
    this.uid = this.route.snapshot.params.uid;

    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    // this.spinner.show();
    this.createForm();
    // this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    const data = {sid: this.sid}; this.isDefault = false;
    this.service.systemProfitLossEvent(this.uid, data).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1) {
      $('#DataTables_profit_loss_event').dataTable().fnDestroy();
      if ( res.userName !== undefined ) {
        if ( res.sport !== undefined ) {
          this.title = res.sport + ' - System Profit Loss From W/L';
        } else {
          this.title = 'Event - System Profit Loss From W/L';
        }
      }
      if (res.data !== undefined) {
        const items = res.data;
        const data: listData[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: listData = new listDataObj();

            cData.id = item.id;
            cData.time = item.time;
            cData.description = item.description;
            cData.eventId = item.eid;
            cData.pl = item.pl;
            cData.comm = item.comm;
            cData.result = item.result;
            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        if (res.total) {
          this.aListTotal = res.total;
        }

        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        // this.loadJs.load('script' , 'assets/js/datatables.init.js');
        this.loadScript();

      }
    }
    this.spinner.hide();
  }

  createForm() {
    this.frm = this.formBuilder.group({
      start: ['', Validators.required],
      end: ['', Validators.required],
      sid: [this.sid],
    });
  }

  submitForm( fType = null ) {
    this.isDefault = false;
    if ( fType != null ) {
      this.spinner.show();
      const data = this.frm.value; data.ftype = fType;
      this.service.systemProfitLossEvent(this.uid, data).subscribe((res) => this.onSearch(res));
    } else {
      if (this.frm.valid) {
        this.spinner.show();
        const data = this.frm.value;
        this.service.systemProfitLossEvent(this.uid, data).subscribe((res) => this.onSearch(res));
      }
    }
  }

  onSearch(res) {
    if (res.status === 1) {
      // this.frm.reset();
      this.onSuccess(res);
    }
  }

  loadScript() {
    // tslint:disable-next-line:only-arrow-functions
    $(document).ready( function() {
      $('#DataTables_profit_loss_event').DataTable({
        "scrollX": true,
        "destroy": true,
        "retrieve": true,
        "stateSave": true,
        "language": {
          "paginate": {
            "previous": "<i class='mdi mdi-chevron-left'>",
            "next": "<i class='mdi mdi-chevron-right'>"
          }
        },
        "drawCallback": function drawCallback() {
          $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
      }); // Complex headers with column visibility Datatable
    });

  }

  excelDownload() {
    const tmpData = [];
    let tmp = {
      NO: '',
      TIME: '',
      DESCRIPTION: '',
      PROFIT_LOSS: '',
      COMM: '',
      RESULT: ''
    };
    tmpData.push(tmp);
    this.aList.forEach((item, index) => {
      const pl = parseFloat(item.pl) !== 0 ? item.pl : '0.0';
      const comm = parseFloat(item.comm) !== 0 ? item.comm : '0.0';
      const i = (index + 1).toString();
      tmp = {
        NO: i,
        TIME: item.time,
        DESCRIPTION: item.description,
        PROFIT_LOSS: pl,
        COMM: comm,
        RESULT: item.result
      };
      tmpData.push(tmp);
    });
    this.excelSheet.exportAsExcelFile(tmpData, 'profit-loss');
  }

  get frmStart() { return this.frm.get('start'); }
  get frmEnd() { return this.frm.get('end'); }

  filters(search: string) {
    this.fType = search;
    this.filter.isFirst = 0;
    switch (search) {
      case 'today' :
        this.filter.start_date = moment().format('YYYY-MM-DD');
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'yesterday' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(1, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'week' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(7, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'month' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(30, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case '3_month' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(90, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
    }
    // this.filterChanged.emit(this.filter);
  }

  setInputDate(sDate: Date, eDate: Date) {
      const data={'start': sDate ,'end' : eDate}
      this.frm.patchValue(data);
      this.submitForm(this.fType);
  }
}

