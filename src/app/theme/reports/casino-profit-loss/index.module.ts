import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CasinoProfitLossComponent } from './index.component';

import {ListComponent} from './list/index.component';
import {ListModule} from './list/index.module';

import {ListMarketDetailComponent} from './listMarketDetail/index.component';
import {ListMarketDetailModule} from './listMarketDetail/index.module';

const routes: Routes = [
  {
    path: '',
    component: CasinoProfitLossComponent,
    children: [
      {
        path: '',
        component: ListComponent
      },
      {
        path: 'market-detail',
        component: ListMarketDetailComponent
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule, ListModule, ListMarketDetailModule
  ], exports: [
    RouterModule
  ], declarations: [ CasinoProfitLossComponent ]
})
export class CasinoProfitLossModule {

}
