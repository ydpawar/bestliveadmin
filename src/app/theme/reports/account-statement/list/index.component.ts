import {Component, OnInit, AfterViewInit, OnDestroy, Injector} from '@angular/core';
import {HistoryService} from '../../../../_api/index';
import {Validators} from '@angular/forms';
import {ExcelService} from '../../../../common/excel.service';
import {BaseComponent} from '../../../../common/commonComponent';
import {AuthIdentityService} from '../../../../_services';
import * as moment from 'moment'; // Momentjs
declare var $;
// tslint:disable-next-line:class-name
interface listData {
  id: string;
  description: string;
  amount: string;
  balance: string;
  time: string;
  remark: string;
  type: string;
  status: string;
  mid: string;
}

// tslint:disable-next-line:class-name
class listDataObj implements listData {
  id: string;
  description: string;
  amount: string;
  balance: string;
  time: string;
  remark: string;
  type: string;
  status: string;
  mid: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [HistoryService]
})

export class ListComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
  title = 'Account Statement';
  breadcrumb: any = [{title: 'Account Statement', url: '/' }, {title: 'List', url: '/' }];

  page = { start: 1, end: 5 };
  user: any;
  uid = '';
  aList: listData[] = [];
  cItem: listData;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_account_statement';
  userName: string;
  total: string;
  lastMonth: string;
  lastMonthUrl: string;

  public filter: any ={};
  fType :string ='';
  constructor(
      inj: Injector,
      private service: HistoryService,
      private excelSheet: ExcelService
  ) {
    super(inj);
    this.uid = this.activatedRoute.snapshot.params.uid;

    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.createForm();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    this.service.accountStatement(this.uid, {}).subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1) {
      $('#DataTables_account_statement').dataTable().fnDestroy();
      if ( res.userName !== undefined ) {
        this.title = 'Account Statement for ' + res.userName;
      }
      if ( res.total !== undefined ) {
        this.total = res.total;
      }
      if ( res.lastMonth !== undefined && res.lastMonthUrl !== undefined ) {
        this.lastMonth = res.lastMonth;
        this.lastMonthUrl = res.lastMonthUrl;
      }

      if ( res.data !== undefined ) {
        const items = res.data;
        const data: listData[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: listData = new listDataObj();

            cData.id = item.id;
            cData.description = item.description;
            cData.amount = item.amount;
            cData.balance = item.balance;
            cData.time = item.updated_on;
            cData.remark = item.remark;
            cData.type = item.type;
            cData.status = item.status;
            cData.mid = item.mid;
            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }

        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        // this.loadJs.load('script' , 'assets/js/datatables.init.js');
        this.initDataTables('DataTables_account_statement');

      }
    }
    this.spinner.hide();
  }

  createForm() {
    this.frm = this.formBuilder.group({
      type: ['', Validators.required],
      start: ['', Validators.required],
      end: ['', Validators.required],
    });
  }

  submitForm(fType = null) {
    if ( fType != null ) {
      this.spinner.show();
      const data = this.frm.value; data.ftype = fType;
      this.service.accountStatement(this.uid, data).subscribe((res) => this.onSearch(res));
    } else {
      if (this.frm.valid) {
        this.spinner.show();
        const data = this.frm.value;
        this.service.accountStatement(this.uid, data).subscribe((res) => this.onSearch(res));
      }
    }
  }

  onSearch(res) {
    if (res.status === 1) {
      // this.frm.reset();
      this.onSuccess(res);
    }
  }

  excelDownload() {
    const tmpData = [];
    let tmp = {
      NO: '',
      DESCRIPTION: '',
      CREDIT: '',
      DEBIT: '',
      BALANCE: '',
      DATE_TIME: '',
      REMARK: ''
    };
    tmpData.push(tmp);
    this.aList.forEach((item, index) => {
      const credit = parseFloat(item.amount) > 0 ? item.amount : '0.00';
      const debit = parseFloat(item.amount) < 0 ? item.amount : '0.00';
      const i = (index + 1).toString();
      tmp = {
        NO: i,
        DESCRIPTION: item.description,
        CREDIT: credit,
        DEBIT: debit,
        BALANCE: item.balance,
        DATE_TIME: item.time,
        REMARK: item.remark
      };
      tmpData.push(tmp);
    });
    this.excelSheet.exportAsExcelFile(tmpData, 'accountstatment');
  }

  get frmType() { return this.frm.get('type'); }
  get frmStart() { return this.frm.get('start'); }
  get frmEnd() { return this.frm.get('end'); }

  filters(search: string) {
    this.fType = search;
    this.filter.isFirst = 0;
    switch (search) {
      case 'today' :
        this.filter.start_date = moment().format('YYYY-MM-DD');
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'yesterday' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(1, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'week' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(7, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'month' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(30, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case '3_month' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(90, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
    }
    // this.filterChanged.emit(this.filter);
  }

  setInputDate(sDate: Date, eDate: Date) {
      const data={'start': sDate ,'end' : eDate}
      this.frm.patchValue(data);
      this.submitForm(this.fType);
  }
}

