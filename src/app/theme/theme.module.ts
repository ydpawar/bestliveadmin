import {NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {ThemeComponent} from './theme.component';
import {Routes, RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';

import {LeftSidebarComponent} from './../public/left-sidebar/left-sidebar.component';

import {RightSidebarComponent} from './../public/right-sidebar/right-sidebar.component';
import {HeaderComponent} from './../public/header/header.component';
import {FooterComponent} from './../public/footer/footer.component';
import {AuthGuard, AuthGuardChild} from './../_guards/auth.guard';
import {AuthIdentityService} from './../_services/auth-identity.service';
import {NgxSpinnerModule, NgxSpinnerService} from 'ngx-spinner';
import {ExcelService} from '../common/excel.service';

const routes: Routes = [
    {
        path: '',
        component: ThemeComponent,
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuardChild],
        children: [
            {
                path: '',
                loadChildren: './home/index.module#HomeModule'
            },
            {
                path: 'dashboard',
                loadChildren: './home/index.module#HomeModule'
            },
            {
                path: 'system-board',
                loadChildren: './system-board/index.module#SystemBoardModule'
            },
            {
                path: 'system-board/event-details',
                loadChildren: './system-event-details/index.module#SystemEventDetailsModule'
            },
            {
                path: 'dashboard/event-details',
                loadChildren: './event-details/index.module#EventDetailsModule'
            },
            {
                path: 'setting',
                loadChildren: './setting/index.module#SettingModule'
            },
            {
                path: 'casino-setting',
                loadChildren: './casino-setting/index.module#CasinoSettingModule'
            },
            {
                path: 'rules',
                loadChildren: './rules/index.module#RulesModule'
            },
            {
                path: 'bank-account',
                loadChildren: './bank-account/index.module#BankAccountModule'
            },
            {
                path: 'users',
                loadChildren: './users/index.module#UsersModule'
            },
            {
                path: 'current-bets',
                loadChildren: './current-bets/index.module#CurrentBetsModule'
            },
            {
                path: 'sport-block-unblock',
                loadChildren: './sport-block-unblock/index.module#SportBlockUnblockModule'
            },
            {
                path: 'market-bet-list',
                loadChildren: './market-bet-list/index.module#MarketBetListModule'
            },
            {
                path: 'system-market-bet-list',
                loadChildren: './system-market-bet-list/index.module#SystemMarketBetListModule'
            },
            {
                path: 'market-bet-list-byuser',
                loadChildren: './market-bet-list-byuser/index.module#MarketBetListByUserModule'
            },
            {
                path: 'settlement-live',
                loadChildren: './settlement-live/index.module#SettlementLiveModule'
            },
            {
                path: 'settlement',
                loadChildren: './settlement/index.module#SettlementModule'
            },
            {
                path: 'client-profit-loss',
                loadChildren: './client-profit-loss/index.module#ClientProfitLossModule'
            },
            {
                path: 'system-settlement',
                loadChildren: './system-settlement/index.module#SystemSettlementModule'
            },
            {
                path: 'reports',
                loadChildren: './reports/index.module#ReportsModule'
            },
            {
                path: 'result',
                loadChildren: './result/index.module#ResultModule'
            },
            {
                path: '**',
                redirectTo: 'dashbord',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '**',
        redirectTo: '404',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes), CommonModule,
        NgxSpinnerModule
    ],
    exports: [RouterModule],
    declarations: [ThemeComponent, HeaderComponent, FooterComponent, LeftSidebarComponent, RightSidebarComponent],
    providers: [AuthIdentityService, ExcelService, NgxSpinnerService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ThemeModule {
}
