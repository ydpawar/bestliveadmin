import { Component, OnInit, AfterViewInit, Injector } from '@angular/core';
import { RulesService } from '../../../_api/index';
import swal from 'sweetalert2';
import {ScriptLoaderService} from '../../../_services';
import { BaseComponent } from 'src/app/common/commonComponent';

declare const $: any;

interface RuleManage {
  id: string;
  categoryname: string;
  sub_category: string;
  rule: string;

  status: {
    text: string;
    class1: string;
    class2: string;
  };
}

class RuleManageObj implements RuleManage {
  id: string;
  categoryname: string;
  sub_category: string;
  rule: string;
  status: {
    text: string;
    class1: string;
    class2: string;
  };
}

@Component({
  selector: 'app-manage',
  templateUrl: './index.component.html',
  providers: [RulesService]
})

export class ManageComponent extends BaseComponent implements OnInit, AfterViewInit {

  title = 'Rule Setting';
  createUrl = '/rules/create/0';
  breadcrumb: any = [{title: 'Rules Setting', url: '/' }, {title: 'Manage', url: '/' }];

  page = {start: 1, end: 5};
  aList: RuleManage[] = [];
  cItem: RuleManage;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_rules';

  constructor( inj: Injector, private service: RulesService, private loadJs: ScriptLoaderService ) {super(inj); }

  ngOnInit() {
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  applyFilters() {
    this.service.manage().subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {

    if (res.status !== undefined && res.status === 1 && res.data !== undefined) {
      $('#DataTables_rules').dataTable().fnDestroy();
      // this.isLoading = false;
      const items = res.data;
      const data: RuleManage[] = [];

      if (items.length) {
        this.aList = [];
        for (const item of items) {
          const cData: RuleManage = new RuleManageObj();

          cData.id = item.id;
          cData.sub_category = item.sub_category;
          cData.categoryname = item.category_name;
          cData.rule = item.rule;
          cData.status = {
            text: item.status === 1 ? 'Active' : 'Inactive',
            class1: item.status === 1 ? 'success' : 'danger',
            class2: item.status === 1 ? 'fa-check' : 'fa-times'
        };

          data.push(cData);
        }
      } else {
        this.isEmpty = true;
      }
        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        this.initDataTables('DataTables_rules');

    }
  }


  changeStatus(item: RuleManage) {
    this.cItem = item;
    const status = item.status.text === 'Active' ? 'Inactive' : 'Active';

    swal.fire({
      title: 'You want to ' + status + ' ' + item.sub_category + '?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
      .then((result) => {
        if (result.value) {
          this.initStatus();
        }
      });
  }


  initStatus() {
    const data = { id: this.cItem.id, status: this.cItem.status.text};
    this.service.status(data).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  doDelete(id) {
    swal.fire({
      title: 'You want to Delete ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Sure!'
    })
        .then((result) => {
          if (result.value) {
            this.initDelete(id);
          }
        });
  }

  initDelete(id) {
    this.service.delete(id).subscribe((res) => this.onChangeStatusSuccess(res));
  }

  onChangeStatusSuccess(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  loadScript() {
    // tslint:disable-next-line:only-arrow-functions
    $(document).ready( function() {
      $('#DataTables_rules').DataTable({
        "scrollX": true,
        "destroy": true,
        "retrieve": true,
        "stateSave": true,
        "language": {
          "paginate": {
            "previous": "<i class='mdi mdi-chevron-left'>",
            "next": "<i class='mdi mdi-chevron-right'>"
          }
        },
        "drawCallback": function drawCallback() {
          $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
      }); // Complex headers with column visibility Datatable
    });

  }

}
