import { Component, OnInit, AfterViewInit, OnDestroy, Injector } from '@angular/core';
import { BankAccountService } from '../../../_api/index';
import { Validators } from '@angular/forms';
import { BaseComponent } from '../../../common/commonComponent';
import { AuthIdentityService } from '../../../_services';
import swal from 'sweetalert2';
import { Location } from '@angular/common';
declare var $;

interface listData {
  id: string;
  name: string;
  description: string;
  amountName: string;
  acNumber: string;
  ifscCode: string;
  date: string;
  type: string;
  status: any;
  utrNo: string;
  payment_gateway_id: string;
}

class listDataObj implements listData {
  id: string;
  name: string;
  description: string;
  amountName: string;
  acNumber: string;
  ifscCode: string;
  date: string;
  type: string;
  status: any;
  utrNo: string;
  payment_gateway_id: string;
}


@Component({
  selector: 'app-bank-list',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [BankAccountService]
})
export class BankAccountManageComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
  title = 'Bank Account Setting';
  createUrl = '/bank-account/create';
  breadcrumb: any = [{ title: 'Bank Account Setting', url: '/' }, { title: 'Manage', url: '/' }];

  page = { start: 1, end: 5 };
  user: any;
  uid = '';
  aList: listData[] = [];
  cItem: listData;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_bank_account';
  userName: string;
  paymentGatewayList: any;
  payId: any;

  constructor(
    inj: Injector,
    private service: BankAccountService,
    private location: Location
  ) {
    super(inj);
    this.uid = this.activatedRoute.snapshot.params.uid;

    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.applyFilters();
    this.createForm();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  applyFilters() {
    this.service.manage().subscribe((res) => this.onSuccess(res));
  }

  onSuccess(res) {
    if (res.status !== undefined && res.status === 1) {
      this.spinner.hide();

      $('#DataTables_bank_account').dataTable().fnDestroy();
      if (res.userName !== undefined) {
        this.title = 'Bank Details for ' + res.userName;
      }
      if (res.data !== undefined) {

        this.paymentGatewayList = res.payment_gateway_list;
        const items = res.data;
        const data: listData[] = [];

        if (items.length > 0) {
          this.aList = [];
          for (const item of items) {
            const cData: listData = new listDataObj();

            cData.id = item.id;
            cData.name = item.name;
            cData.payment_gateway_id = item.payment_gateway_id
            cData.amountName = item.account_name;
            cData.acNumber = item.account_number;
            cData.date = item.created_at;
            cData.ifscCode = item.ifsc_code;
            cData.utrNo = item.utr_number;
            cData.status = item.status;
            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }
        this.aList = data;
        this.page.end = this.page.end + res.data.length - 1;
        this.initDataTables('DataTables_bank_account');
      }
    }
  }

  doStatusDelete(id, status) {
    swal.fire({
      title: 'Are you sure to delete ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        const data = { id, status };
        this.service.status(data).subscribe((res) => this.onSuccessStatusUpdate(res));
      }
    });
  }

  doStatusUpdate(id, status) {
    swal.fire({
      title: 'Are you sure to change this status ? ',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        // tslint:disable-next-line:triple-equals
        if ( status == 1 ) { status = 0; } else { status = 1; }
        const data = { id, status };
        this.service.status(data).subscribe((res) => this.onSuccessStatusUpdate(res));
      }
    });
  }

  onSuccessStatusUpdate(res) {
    if (res.status === 1) {
      this.applyFilters();
    }
  }

  onCancel() {
    $('.update-bank-detail').modal('hide');
    $('body').removeClass('modal-open');
  }

  updateBankDetails(item) {
    this.payId = item.payment_gateway_id
    this.frm.reset();
    $('.update-bank-detail').modal('show');
    $('body').removeClass('modal-open');

    this.frm.patchValue({
      id: item.id,
      payment_gateway_id: item.payment_gateway_id,
      account_name: item.amountName,
      account_number: item.acNumber,
      ifsc_code: item.ifscCode,
      // utr_number: item.utrNo
    });

  }

  createForm() {
    this.frm = this.formBuilder.group({
      id: ['', Validators.required],
      payment_gateway_id: ['', Validators.required],
      account_name: ['', Validators.required],
      account_number: ['', Validators.required],
      ifsc_code: [0, Validators.required],
      // utr_number: ['', Validators.required],
    });
  }

  submitForm() {
    const data = this.frm.value;
    if (this.frm.valid) {
      this.service.update(data).subscribe((res) => this.onSuccessCreate(res));
    }
  }

  onSuccessCreate(res) {
    if (res.status === 1) {
      this.applyFilters();
      $('.update-bank-detail').modal('hide');
      $('body').removeClass('modal-open');
      this.frm.reset();
    }
  }

  paymentGatewayOptionChange(e) {
    this.frm.value.payment_gateway_id = e;
  }

  get frmPayGatName() { return this.frm.get('payment_gateway_id'); }
  get frmACname() { return this.frm.get('account_name'); }
  get frmACNo() { return this.frm.get('account_number'); }
  get frmIFSCcode() { return this.frm.get('ifsc_code'); }
  // get frmUTRNo() { return this.frm.get('utr_number'); }

}

