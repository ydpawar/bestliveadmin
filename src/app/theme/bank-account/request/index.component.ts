import { Component, OnInit, AfterViewInit, OnDestroy, Injector } from '@angular/core';
import { BankAccountService } from '../../../_api/index';
import { Validators } from '@angular/forms';
import { ExcelService } from '../../../common/excel.service';
import { BaseComponent } from '../../../common/commonComponent';
import { AuthIdentityService } from '../../../_services';
import * as moment from 'moment'; // Momentjs
import swal from "sweetalert2";
import { Location } from '@angular/common';
declare var $;

interface listData {
  id: string;
  uId: string;
  name: string;
  description: string;
  amount: string;
  acNumber: string;
  acName: string;
  ifscCode: string;
  sender_name: string;
  receiverName: string;
  utr_no: string;
  date: string;
  type: string;
  notifyStatus: string;
}

class listDataObj implements listData {
  id: string;
  uId: string;
  name: string;
  description: string;
  amount: string;
  acNumber: string;
  acName: string;
  ifscCode: string;
  sender_name: string;
  receiverName: string;
  utr_no: string;
  date: string;
  type: string;
  notifyStatus: string;
}

@Component({
  selector: 'app-bank-request',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [BankAccountService]
})

export class BankAccountRequestComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
  title = 'Bank Account Request';
  breadcrumb: any = [{ title: 'Bank Account', url: '/' }, { title: 'Request', url: '/' }];

  page = { start: 1, end: 5 };
  user: any;
  uid = '';
  aList: listData[] = [];
  cItem: listData;
  isEmpty = false;
  isLoading = false;
  dataTableId = 'DataTables_transaction_report';
  userName: string;
  public filter: any = {};
  fType: string;

  constructor(
    inj: Injector,
    private service: BankAccountService,
    private excelSheet: ExcelService,
    private _location: Location
  ) {
    super(inj);
    this.createForm();
    this.uid = this.activatedRoute.snapshot.params.uid;

    const auth = new AuthIdentityService();
    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();

      // tslint:disable-next-line:triple-equals
      if (this.user.role == 'SV' || this.user.role == 'SA') {
        this._location.back();
      }
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.applyFilters();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    window.localStorage.removeItem(this.dataTableId);
  }

  mudouPagina(event) {
    this.page = event.valor;
    this.applyFilters();
  }

  applyFilters() {
    if (this.frm.value.start_date && this.frm.value.start_date) {
      const data = this.frm.value; data.isFirst = 0;
      this.service.bankRequest(this.uid, data).subscribe((res) => this.onSuccess(res));
    } else {
      const data = this.frm.value; data.isFirst = 1;
      this.service.bankRequest(this.uid, data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status !== undefined && res.status === 1) {
      this.spinner.hide();

      $('#DataTables_transaction_report').dataTable().fnDestroy();
      if (res.userName !== undefined) {
        this.title = 'Transaction Report for ' + res.userName;
      }
      if (res.data !== undefined) {
        const items = res.data;
        const data: listData[] = [];

        if (items.length > 0) {
          this.isEmpty = false;
          this.aList = [];
          for (const item of items) {
            const cData: listData = new listDataObj();

            cData.id = item.id;
            cData.uId = item.user_id;
            cData.name = item.username;
            cData.description = item.description;
            cData.amount = item.amount;
            cData.acName = item.account_name;
            cData.acNumber = item.account_number;
            cData.sender_name = item.sender_name
            cData.receiverName = item.receiver_name
            cData.utr_no = item.utr_number
            cData.date = item.created_on;
            cData.type = item.request_type;
            cData.ifscCode = item.ifsc_code;
            cData.notifyStatus = item.notification_status;
            data.push(cData);
          }
        } else {
          this.isEmpty = true;
        }
        this.aList = data;
        this.page.end = this.page.end + items.length - 1;
        // this.initDataTables('DataTables_transaction_report');
        this.loadScript();
      }
    }
  }

  createForm() {
    this.frm = this.formBuilder.group({
      type: ['', Validators.required],
      start_date: ['', Validators.required],
      end_date: ['', Validators.required],
    });
  }

  submitForm(fType = null) {
    if (fType != null) {
      this.spinner.show();
      const data = this.frm.value; data.ftype = fType; data.isFirst = 0;
      this.service.bankRequest(this.uid, data).subscribe((res) => this.onSearch(res));
    } else {
      if (this.frm.valid) {
        this.spinner.show();
        const data = this.frm.value; data.isFirst = 0
        this.service.bankRequest(this.uid, data).subscribe((res) => this.onSearch(res));
      }
    }
  }

  onSearch(res) {
    if (res.status === 1) {
      // this.frm.reset();
      this.onSuccess(res);
    }
  }

  get frmType() { return this.frm.get('type'); }
  get frmStart() { return this.frm.get('start_date'); }
  get frmEnd() { return this.frm.get('end_date'); }

  filters(search: string) {
    this.fType = search;
    this.filter.isFirst = 0;
    switch (search) {
      case 'deposit':
        this.filter.start_date = moment().format('YYYY-MM-DD');
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.setInputType('deposit');
        break;
      case 'withdraw':
        this.filter.start_date = moment().format('YYYY-MM-DD');
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.setInputType('withdraw');
        break;
      case 'today':
        this.filter.start_date = moment().format('YYYY-MM-DD');
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'yesterday':
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(1, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'week':
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(7, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'month':
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(30, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case '3_month':
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(90, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
    }
    // this.filterChanged.emit(this.filter);
  }

  setInputDate(sDate: Date, eDate: Date) {
    const data = { 'start_date': sDate, 'end_date': eDate }
    this.frm.patchValue(data);
    this.submitForm(this.fType);
  }

  setInputType(type) {
    const data = { type };
    this.frm.patchValue(data);
    this.submitForm(this.fType);
  }

  readNotify(item, status, title, type) {
    const data = { id: item.id, notification_status: status };
    const data1 = { id: item.id, notification_status: status, is_accepted: type };

    swal.fire({
      title: 'Are you sure to want ' + title + ' ?',
      // text: 'Are you sure to logout?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        if (type) {
          this.service.readNotification(data1).subscribe((res) => this.onSuccessReadNotify(res, item));
        } else {
          this.service.readNotification(data).subscribe((res) => this.onSuccessReadNotify(res, item));
        }
      }
    });
  }

  onSuccessReadNotify(res, item) {
    if (res.status === 1) {
      window.localStorage.setItem('title', item.name);
      if (item.type === 'withdraw') {
        window.sessionStorage.setItem('inputValue', item.amount);
        window.sessionStorage.setItem('reqId', item.id);
        this.router.navigate(['/users/action/withdrawal/' + item.uId]);
      }
      if (item.type === 'deposit') {
        window.sessionStorage.setItem('inputValue', item.amount);
        this.router.navigate(['/users/action/deposit/' + item.uId]);
      }
      if (item.type === 'forgotpassword') {
        this.router.navigate(['/users/action/reset-password/' + item.uId]);
      }
      this.applyFilters();
    }
  }

  loadScript() {
    $(document).ready(function () {
      $('#DataTables_transaction_report').DataTable({
        'scrollX': true,
        'destroy': true,
        'retrieve': true,
        'stateSave': true,
        'lengthMenu': [
          [50,100,150,200],
          ['50','100','150','200']
        ],
        'language': {
          'paginate': {
            'previous': '<i class=\'mdi mdi-chevron-left\'>',
            'next': '<i class=\'mdi mdi-chevron-right\'>'
          }
        },
        'drawCallback': function drawCallback() {
          $('.dataTables_paginate > .pagination').addClass('pagination-rounded');
        }
      }); // Complex headers with column visibility Datatable
    });

  }

}

