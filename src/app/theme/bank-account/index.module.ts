import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BankAccountComponent } from './index.component';
import { BankAccountManageComponent } from './manage/index.component';
import { BankAccountCreateComponent } from './create/index.component';
import { BankAccountRequestComponent } from './request/index.component';

const routes: Routes = [
  {
    path: '',
    component: BankAccountManageComponent
  },
  {
    path: 'manage',
    component: BankAccountManageComponent
  },
  {
    path: 'create',
    component: BankAccountCreateComponent
  },
  {
    path: 'request',
    component: BankAccountRequestComponent
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [BankAccountComponent, BankAccountManageComponent, BankAccountCreateComponent, BankAccountRequestComponent]
})
export class BankAccountModule {

}
