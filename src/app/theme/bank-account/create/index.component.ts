import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BankAccountService } from 'src/app/_api';
import { AuthIdentityService } from 'src/app/_services';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-bank-create',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [BankAccountService]
})

export class BankAccountCreateComponent implements OnInit {
  title = 'Bank Account Create';
  breadcrumb: any = [{ title: 'Bank Account', url: '/' }, { title: 'Create', url: '/' }];

  user: any;
  frm: FormGroup;
  userError: string;
  parentUser: string;
  plError: number;
  tmpplError: number;
  belance: number;
  tmpbelance: number;
  paymentGatewayList: any;
  payId: any;

  constructor(
    private formBuilder: FormBuilder,
    private service: BankAccountService,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private spinner: NgxSpinnerService
  ) {
    const auth = new AuthIdentityService();

    if (auth.isLoggedIn()) {
      this.user = auth.getIdentity();
    }
  }

  ngOnInit() {
    this.spinner.show();
    this.paymentGatewaysList();
    this.createForm();
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
  }

  paymentGatewaysList() {
    this.service.getPaymentOptions().subscribe((res) => { this.onSuccessList(res); });
  }
  onSuccessList(res) {
    if (res.status !== undefined && res.status === 1) {
      this.spinner.hide();
      if (res.data !== undefined) {
        this.paymentGatewayList = res.data
      }
    }
  }

  createForm() {
    this.frm = this.formBuilder.group({
      payment_gateway_id: ['', Validators.required],
      account_name: ['', Validators.required],
      account_number: ['', Validators.required],
      ifsc_code: [0, Validators.required],
      // utr_number: ['', Validators.required],
    });
  }

  submitForm() {
    const data = this.getFormData();
    if (this.frm.valid) {
      this.service.create(data).subscribe((res) => this.onSuccess(res));
    }
  }

  onSuccess(res) {
    if (res.status === 1) {
      this.frm.reset();
      this.location.back();
      // this.router.navigate(['/manage']);
    }
  }

  onCancel() {
    this.location.back();
  }

  paymentGatewayOptionChange(e) {
    this.payId = e;
    this.frm.value.payment_gateway_id = e;
  }

  getFormData() {
    const data = this.frm.value;
    return data;
  }

  get frmPayGatName() { return this.frm.get('payment_gateway_id'); }
  get frmACname() { return this.frm.get('account_name'); }
  get frmACNo() { return this.frm.get('account_number'); }
  get frmIFSCcode() { return this.frm.get('ifsc_code'); }
  // get frmUTRNo() { return this.frm.get('utr_number'); }

}
