import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SportBlockUnblockComponent } from './index.component';


const routes: Routes = [
  {
    path: '',
    component: SportBlockUnblockComponent,
  },
  {
    path: ':uid',
    component: SportBlockUnblockComponent,
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes),
    FormsModule, ReactiveFormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    SportBlockUnblockComponent
  ]
})
export class SportBlockUnblockModule {
}
