import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './public/login/login.component';
import {FormsModule} from '@angular/forms';

const routes: Routes = [
    {
      path: 'login',
      component: LoginComponent,
    },
    {
      path: '',
      loadChildren: './theme/theme.module#ThemeModule',
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), FormsModule],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
