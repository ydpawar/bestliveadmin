import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpRequestModel, HttpRequestProcessDisplay, RequestOption, HttpService } from './../../_services/index';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private http: HttpService) { }

  getTransaction(uid,data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    if (uid) {
      httpRequestModel.url = 'reports/transaction-report/' + uid;
    } else {
      httpRequestModel.url = 'reports/transaction-report';
    }
    httpRequestModel.method = 'Post';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  readNotify(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'read-notification';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }
}
