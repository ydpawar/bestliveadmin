import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpRequestModel, HttpRequestProcessDisplay, RequestOption, HttpService } from './../../_services/index';

@Injectable()
export class SubAdminService {
  constructor(private http: HttpService) { }


  manage(): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'app-user/sub-admin/manage'; // users controller/
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  reference(uid): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'app-user/sub-admin/reference/' + uid; // users controller/
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  create(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'app-user/sub-admin/create'; // users controller/
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = true;

    return this.http.init(httpRequestModel, processDisplay);
  }

  checkUserData(): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'app-user/action/check-user-data'; // users controller/
    httpRequestModel.method = 'GET';
    httpRequestModel.body = {};
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

}
