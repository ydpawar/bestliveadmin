import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequestModel, HttpRequestProcessDisplay, HttpService } from 'src/app/_services';

@Injectable({
  providedIn: 'root'
})
export class ResultService {

  constructor(private http: HttpService) { }


  result(id, data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    // tslint:disable-next-line:triple-equals
    if (id) {
      httpRequestModel.url = 'market-result/' + id;
    } else {
      httpRequestModel.url = 'market-result';
    }
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  gameOver(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'result-game-over';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  gameRecall(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'result-game-recall';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }

  abundant(data): Observable<JSON> {
    const httpRequestModel = new HttpRequestModel();
    httpRequestModel.url = 'result-game-abundant';
    httpRequestModel.method = 'POST';
    httpRequestModel.body = data;
    httpRequestModel.header = 'Auth';

    const processDisplay = new HttpRequestProcessDisplay();
    processDisplay.loader = true;
    processDisplay.loaderMessage = 'Loading Data!';
    processDisplay.responseMessage = false;

    return this.http.init(httpRequestModel, processDisplay);
  }
}
